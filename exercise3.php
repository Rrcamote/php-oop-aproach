


<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>CodePen - Material Design Login and Registration Form</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Montserrat:400,600'><link rel="stylesheet" href="./style.css">
<link rel="stylesheet" href="login.css">
</head>
<body>

<div class="panel_blur"></div>
  <div class="panel">
    <div class="panel__form-wrapper">
       

      <ul class="panel__headers">
        <li class="panel__header active"><a href="#login-form" class="panel__link text-white" role="button">Email Validation</a></li>
      </ul>

      <div class="panel__forms">

        <form class="form panel__login-form" id="login-form" method="post">
          <div class="form__row">
            <input type="text" id="email" class="form__input" name="email" >
            <label for="email" class="form__label">E-mail</label>
            <span class="form__error"></span>
          </div>
          <div class="form__row">
            <input type="submit" id="sub" class="form__submit" name="submit" value="Submit">
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js'></script><script  src="./script.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>
<script>
    $(document).ready(function(){
       
})
</script>
</html>


<?php

trait emailValidate {

  public function emailVal() {

    if(isset($_POST['submit'])){

  
      $email =$_POST['email'];
     
      if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
       echo("<h1 style ='color:blue;'> $email is a valid email address </h1>");
     } else {
       echo("<h1 style ='color:black;'>$email is not a valid email address</h1>");
     }
     
     
     }



    }
  }
  
  class result {
    use emailValidate;
  }
  
  $obj = new result();
  $obj->emailVal();



?>
