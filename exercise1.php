<?php
// public class Difference(){
//     $Date1 = new DateTime("2007-03-24");
//     $date2 = new DateTime("2009-06-26");
//     $interval = $Date1->diff($date2);
//  return   echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days "; 
    
    // shows the total amount of days (not divided into years, months and days like above)
//  return   echo "difference " . $interval->days . " days ";
    
// }

?>

<?php

class Calculate{
    public $Date1 ;
    public $Date2;

    function __construct($Date1,$Date2){
        $this->Date1 = $Date1;
        $this->Date2 = $Date2;
    }
    function get_Date1(){
        return $this->Date1." - Date1";
    }
    function get_Date2(){
        return $this->Date2." - Date2";
        
    }
    function get_Difference(){      
       $difference = (new DateTime($this->Date1))->diff (new DateTime($this->Date2));
       return "Difference : " . $difference->y . " years, " . $difference->m." months, ".$difference->d." days ";
    } 
}
$dateTime = new Calculate("2000-05-12","2021-4-30");
echo $dateTime->get_Date1();
echo "<br>";
echo $dateTime->get_Date2();
echo "<br>";
echo $dateTime->get_Difference();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    
</body>
</html>